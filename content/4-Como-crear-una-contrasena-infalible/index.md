#¿Cómo crear una contraseña infalible?

##1. Pasos fáciles para evitar las debilidades más comunes

- Visita ¿Qué tan segura es mi contraseña? para probar qué tan fuertes son tus contraseñas
https://howsecureismypassword.net/

- Nunca recicles/re-utilices contraseñas

- Crea una contraseña nueva/diferente para cada sitio en el que te inscribas

- No permitas que tu navegador guarde tus contraseñas.

- Piensa cómo usar frases o contraseñas más largas en vez de una palabra secreta.

- Usa letras mayúsculas y minúsculas, números, símbolos. Los administradores de contraseñas usualmente tienen un generador de contraseñas para crear contraseñas complejas.

##2. Usa un administrador de contraseñas

El software de Administración de Contraseñas almacena versiones cifradas de tus contraseñas para que puedas usar una única contraseña segura para cada servicio sin tener que recordar cada una de las contraseñas. Hay muchos Administradores deContraseñas gratuitos de donde elegir, por ejemplo, KeePass, LastPass,

*Keepass tiene versiones actualizadas y con soporte.
Para computadora, KeePassXC https://keepassxc.org/  
Para celular, minikeepass (IOS) y KeePass2Android*

##3.¿Cómo administrar tus contraseñas?

Ha habido debates sobre la frecuencia con la que deberíamos cambiar nuestras contraseñas. También se recomienda que cambies tu contraseña cada 3-9 meses. Te recomendamos que cambies tu contraseña tan seguido como sea necesario para que te sientas segura.

##4. Autenticación de dos factores o verificación de 2 pasos (2FA)

La autenticación de dos factores (2FA) hace que sea más difícil para cualquiera, intentar acceder ilegalmente a tus cuentas. En vez de iniciar la sesión en tus cuentas usando un solo paso (tu contraseña), ingresas una segunda pieza de información, por ej. un código corto que es enviado en un mensaje por correo electrónico, o por mensaje de texto; o generado por una aplicación en tu teléfono móvil. Consulta qué sitios te permiten habilitar la autenticación de dos factores aquí https://twofactorauth.org/
