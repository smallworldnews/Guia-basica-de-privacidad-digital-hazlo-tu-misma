#Cómo proteger tu navegador

![](images/5-gif-cat.gif)

#Chrome. Safari. Firefox. Explorer.

Primero, prueba qué tan seguro es tu actual navegador con Panopticlick
https://panopticlick.eff.org/

![](images/5-cover_laptop_cam.gif)

#Configuración de Privacidad del Navegador - No Seguimiento y Ventanas Privadas de Navegación

#No Seguimiento

En Chrome
> Configuración > Mostrar Configuración Avanzada > > Privacidad > Enviar solicitud de “No Seguimiento” con tu tráfico de navegación

![](images/5-chrome-donottrack.gif)

En Firefox
> Opciones > Privacidad> Administrar tu configuración de “No Seguimiento”

![](images/5-Firefox-donottrack.gif)

En Safari
> Menú > Preferencias > Privacidad > Seguimiento de sitios web > Pedir a los sitios web no seguirme

![](images/5-do-not-track-safari.jpg)

En Internet Explorer
> Herramientas (Alt + X) > Seguridad >Activar la protección de rastreo > Habilitar

![](images/5-explorer-donottrack.gif)

##Navegadores y Motores de Búsqueda Alternativos

- Brave es un navegador que bloquea automáticamente anuncios y rastreadores https://brave.com/
- Tor es un software de navegación que te permite navegar de forma anónima https://www.torproject.org/
- DuckDuckGo es un motor de búsqueda que respeta tu privacidad y no rastrea a los usuarios
https://duckduckgo.com/about
- StartPage es un motor de búsqueda que respeta tu privacidad y no registra tu dirección IP y tus búsquedas.
https://www.startpage.com/

##Modo Incógnito

El Modo Incógnito en la web evita que Google Chrome guarde un registro de tus visitas o descargas.

Personaliza y controla Google Chrome

> Nueva ventana de incógnito
O mantén oprimido: Ctrl + Mayús + N

Nueva Ventana Privada (Firefox)

>Nueva Ventana Privada o  abre una ventana de navegación privada en Firefox.
Abrir menú > Nueva Ventana Privada
O mantén oprimido: Ctrl + Mayús + P

Nueva Ventana Privada (Safari)

Nueva Ventana Privada hace que Safari deje de rastrear qué páginas web estás viendo.

>Menú archivo > Nueva Ventana Privada
O mantén oprimido: Comando + Mayús + N

##Usa una VPN

Una VPN, o Red Privada Virtual, es una manera de ocultar tu dirección IP y cifrar todo tu tráfico en internet para que nadie pueda averiguar lo que estás viendo en línea.

VPN gratuitas:

- OkayFreedom (por favor ten en cuenta: la versión gratuita de OkayFreedom es financiada por publicidad).

- TunnelBear. Puedes encontrar extensiones de Chrome gratuitas y premium.

- Y complementos de VPN para Firefox gratuitos/premium en

> Menú Abrir > Complementos > Obtener complementos > escribe “VPN” en la barra de búsqueda.


##Tor

El navegador Tor está diseñado para ayudarte a mantenerte anónimo y es una simple pero efectiva manera de navegar en internet sin revelar tu identidad a nadie Descárgalo en: https://www.torproject.org/projects/torbrowser.html

![](images/5-SurfingTOR.gif)

##Extensiones para Navegadores

Las extensiones de los navegadores (complementos/programas adicionales),son programas que extienden o personalizan un navegador de internet.

Las extensiones pueden ser utilizadas para proteger tu privacidad al bloquear a terceros para que no hagan seguimiento de tu actividad en línea.

ADVERTENCIA: las extensiones también pueden ser maliciosas, verifica que las descargues de sitios oficiales y pon atención a los permisos y accesos que te solicita.

##Extensiones recomendadas

- HTTPS Everywhere (Protocolo de Transferencia de hipertexto con Secure Sockets Layer) asegura y codifica tu tráfico en internet).

- Privacy Badger bloquea el seguimiento de cookies.

- uBlock Origin es un bloqueador de anuncios de publicidad.

##Borrar tu historial de navegación

>Chrome > History > Borrar datos de navegación
Firefox > Options > Privacidad > Historial

Limpiadores recomendados para tu computador:
CCleaner
BleachBit
