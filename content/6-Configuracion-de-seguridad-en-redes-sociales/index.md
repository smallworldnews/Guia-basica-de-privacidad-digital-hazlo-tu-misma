#Configuración de seguridady privacidad en redes sociales

![](images/6-sadcat.png)

##Facebook

- Revisa la configuración de seguridad y privacidad de tu perfil en Facebook

https://www.facebook.com/safety/

*Checkup de privacidad:*

- ¿Cómo y qué información ve alguien cuando entra a tu perfil?
- ¿Quiénes pueden tus publicaciones?
- ¿Quién puede poneerse en contacto y compartir información contigo ?
- ¿Cómo evito que alguien me moleste? Prueba la opción de **bloqueo de usuarios y mensajes desde la configuración de privacidad de tu cuenta.**

*Checkup de seguridad:*

- Contraseñas seguras
- Alertas de inicio de sesión
- Cerrar sesión en navegadores y aplicaciones que ya no usas
- Verificación o autenticación de 2 pasos

##Twitter

- Revisa la configuración de seguridad y privacidad de tu perfil en Twitter

Twitter tiene algunas herramientas para actuar ante el acoso digital.

- Silenciar: esta función oculta los tuits o notificaciones de otra persona sin que ella lo sepa.
- Bloquear: Si bloqueas a una cuenta, no podrá seguirte, contactarte o ver tus tuits.
- Denunciar: Al denunciar una cuenta podrás reportar su comportamiento abusivo. Twitter puede eliminar el tuit denunciado o bloquear al perfil.

##Dispositivos

##Cómo asegurar tu teléfono

##Android

Desactiva la conexión inalámbrica y la ubicación GPS (en Servicios de Ubicación) y los datos móviles, esto se puede encontrar en:

> Configuración > Personal > Ubicación.

Nota: Solo enciende la configuración de ubicación cuando la necesites. Es importante tener estos servicios apagados de manera predeterminada pues esto disminuirá el riesgo de que tu ubicación sea rastreada . (Fuente: https://securityinabox.org/en/guide/basic-setup/android)

##iPhone

Desactiva la conexión inalámbrica y la ubicación GPS:

>Configuración > Privacidad > Servicios de Ubicación

Puedes desactivar todos los Servicios de Ubicación usando el deslizador, o desactivar cada punto o aplicación que utiliza tu ubicación usando los deslizadores individuales de tu dispositivo. Para deshabilitar los Servicios de Ubicación para todos los sitios web, desactiva los Servicios de Ubicación para la aplicación Safari.

##Cifrando tus Dispositivos

##Android

>Configuración > Seguridad > Cifrar Dispositivo

Tomará una hora cifrar tu dispositivo Android, así que asegurate que tu batería está cargada.

Necesitarás introducir una contraseña para cifrar tu dispositivo cada vez que lo enciendas.

##iPhone

Todos los iPhones están cifrados de forma predeterminada. Sin embargo, deberías asegurarte de usar una contraseña larga para que resulte más difícil de acceder.

##Aplicaciones de mensajería

##Whatsapp

Los mensajes de WhatsApp son cifrados de extremo a extremo (es decir, el contenido de los mensajes que envías, solo puede ser visto por ti y por el receptor), sin embargo los metadatos sobre con quién chateas y por cuánto tiempo son recolectados y almacenados.

##Signal

Signal de Open Whisper Systems es una aplicación cifrada gratuita y de acceso abierto de llamadas de voz y mensajería instantánea para Android, iOS y Computadores de Escritorio.

Esta aplicación utiliza cifrado de extremo a extremo para garantizar todas las comunicaciones a los otros usuarios de Signal. Signal puede ser utilizado para enviar y recibir de forma cifrada mensajes instantáneos, mensajes de grupos, adjuntos y mensajes multimedia.
