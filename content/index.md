#Guía básica de privacidad virtual - Hazlo tú misma

![](images/guia-privacidad-diy-spanishcover.png)

Deja que el gato codificador de Chayn te muestre cómo alguien te puede estar siguiendo el rastro y qué puedes hacer al respecto.

Estás leyendo la guía básica; también puedes leer la guía avanzada en inglés aquí: https://chayn.gitbooks.io/advanced-diy-privacy-for-every-woman/content/
